
#include <sigc++/class_slot.h>
#include <glibmm.h>


namespace
{

struct DispatcherTest : public SigC::Object
{
  DispatcherTest();

  void thread_function1();
  void thread_function2();

  void on_increment1();
  void on_increment2();

  Glib::RefPtr<Glib::MainLoop>  mainloop;
  Glib::Dispatcher              dispatcher1;
  Glib::Dispatcher              dispatcher2;
  unsigned int                  counter1;
  unsigned int                  counter2;
};

DispatcherTest::DispatcherTest()
:
  mainloop (Glib::MainLoop::create(true)),
  counter1 (0),
  counter2 (0)
{
  dispatcher1.connect(SigC::slot(*this, &DispatcherTest::on_increment1));
  dispatcher2.connect(SigC::slot(*this, &DispatcherTest::on_increment2));
}

void DispatcherTest::thread_function1()
{
  for(int i = 0; i < 100000; ++i)
    dispatcher1();
}

void DispatcherTest::thread_function2()
{
  for(int i = 0; i < 100000; ++i)
    dispatcher2();
}

void DispatcherTest::on_increment1()
{
  if(++counter1 == 200000 && counter2 == 200000)
    mainloop->quit();
}

void DispatcherTest::on_increment2()
{
  if(++counter2 == 200000 && counter1 == 200000)
    mainloop->quit();
}

} // anonymous namespace


int main(int, char**)
{
  using Glib::Thread;

  Glib::thread_init();

  DispatcherTest test;

  Thread* thread1 = Thread::create(SigC::slot_class(test, &DispatcherTest::thread_function1), true);
  Thread* thread2 = Thread::create(SigC::slot_class(test, &DispatcherTest::thread_function1), true);
  Thread* thread3 = Thread::create(SigC::slot_class(test, &DispatcherTest::thread_function2), true);
  Thread* thread4 = Thread::create(SigC::slot_class(test, &DispatcherTest::thread_function2), true);

  test.mainloop->run();

  thread1->join();
  thread2->join();
  thread3->join();
  thread4->join();

  return 0;
}

