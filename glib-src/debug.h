// -*- c++ -*-
#ifndef _GLIBMM_DEBUG_H
#define _GLIBMM_DEBUG_H

/* $Id$ */

/* Copyright 2002 The gtkmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib/gmacros.h>
#include <glibmmconfig.h>

// Some stuff that's useful when debugging gtkmm internals:

#ifdef GTKMM_DEBUG_REFCOUNTING

#include <glib/gmessages.h>

/* We can't use G_GNUC_PRETTY_FUNCTION because it's always disabled in C++,
 * even though __PRETTY_FUNCTION__ works fine in C++ as well if you use it
 * right (i.e. concatenation with string literals isn't allowed).
 */
#ifdef __GNUC__
#define GTKMM_GNUC_PRETTY_FUNCTION __PRETTY_FUNCTION__
#else
#define GTKMM_GNUC_PRETTY_FUNCTION ""
#endif

#define GTKMM_DEBUG_REFERENCE(cppInstance, cInstance)                               \
    G_STMT_START{                                                                   \
      void *const cppInstance__ = (void*) (cppInstance);                            \
      void *const cInstance__   = (void*) (cInstance);                              \
      g_log(G_LOG_DOMAIN,                                                           \
            G_LOG_LEVEL_DEBUG,                                                      \
            "file %s: line %d (%s):\n"                                              \
            "ref: C++ instance: %p; C instance: %p, ref_count = %u, type = %s",   \
            __FILE__,                                                               \
            __LINE__,                                                               \
            GTKMM_GNUC_PRETTY_FUNCTION,                                             \
            cppInstance__,                                                          \
            cInstance__,                                                            \
            G_OBJECT(cInstance__)->ref_count,                                       \
            G_OBJECT_TYPE_NAME(cInstance__));                                       \
    }G_STMT_END

#define GTKMM_DEBUG_UNREFERENCE(cppInstance, cInstance)                             \
    G_STMT_START{                                                                   \
      void *const cppInstance__ = (void*) (cppInstance);                            \
      void *const cInstance__   = (void*) (cInstance);                              \
      g_log(G_LOG_DOMAIN,                                                           \
            G_LOG_LEVEL_DEBUG,                                                      \
            "file %s: line %d (%s):\n"                                              \
            "unref: C++ instance: %p; C instance: %p, ref_count = %u, type = %s", \
            __FILE__,                                                               \
            __LINE__,                                                               \
            GTKMM_GNUC_PRETTY_FUNCTION,                                             \
            cppInstance__,                                                          \
            cInstance__,                                                            \
            G_OBJECT(cInstance__)->ref_count,                                       \
            G_OBJECT_TYPE_NAME(cInstance__));                                       \
    }G_STMT_END

#else

#define GTKMM_DEBUG_REFERENCE(cppInstance,cInstance)    G_STMT_START{ (void)0; }G_STMT_END
#define GTKMM_DEBUG_UNREFERENCE(cppInstance,cInstance)  G_STMT_START{ (void)0; }G_STMT_END

#endif /* GTKMM_DEBUG_REFCOUNTING */

#endif /* _GLIBMM_DEBUG_H */

