/* $Id$ */

/* Copyright (C) 2002 The gtkmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gdkmm/pixmap.h>
#include <gdk/gdkpixmap.h>

_DEFS(gdkmm,gdk)
_PINCLUDE(gdkmm/private/pixmap_p.h)

namespace Gdk
{

class Window;

// In GDK, GdkBitmap has the same type as GdkPixmap.  Both are typedefed
// to their base type GdkDrawable, but the real type is GdkPixmapObject.
// A GdkBitmap is a GdkPixmap with a depth of 1.

class Bitmap : public Gdk::Pixmap
{
  // The Bitmap type only really exists in gtkmm.  We fake its existence
  // with a custom Gdk::Pixmap_Class::wrap_new() -- see comments there.
  _CLASS_GENERIC(Bitmap, GdkBitmap)

protected:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  explicit Bitmap(GdkBitmap* castitem);
  friend class Gdk::Pixmap_Class;
#endif

  Bitmap(const char* data, int width, int height);

  // TODO: should take a Glib::RefPtr<Gdk::Drawable> instead.
  Bitmap(const Glib::RefPtr<Window>& window, const char* data, int width, int height);

public:
  _WRAP_CREATE(const char* data, int width, int height)

  // TODO: should take a Glib::RefPtr<Gdk::Drawable> instead.
  _WRAP_CREATE(const Glib::RefPtr<Window>& window, const char* data, int width, int height)
};

} // namespace Gdk

