#include <iostream>
#include <gtkmm/window.h>
#include <gtkmm/main.h>
#include <gtkmm/button.h>

// Gtk-- version of the "pixmap" example from the gtk+ tutorial

/* XPM data of Open-File icon */
static const char * xpm_data[] = {
  "16 16 3 1",
  "       c None",
  ".      c #000000000000",
  "X      c #FFFFFFFFFFFF",
  "                ",
  "   ......       ",
  "   .XXX.X.      ",
  "   .XXX.XX.     ",
  "   .XXX.XXX.    ",
  "   .XXX.....    ",
  "   .XXXXXXX.    ",
  "   .XXXXXXX.    ",
  "   .XXXXXXX.    ",
  "   .XXXXXXX.    ",
  "   .XXXXXXX.    ",
  "   .XXXXXXX.    ",
  "   .XXXXXXX.    ",
  "   .........    ",
  "                ",
  "                "
};

class Pixmap : public Gtk::Window
{
public:
  Pixmap();

protected:
  //Signal handler:
  virtual void on_button_clicked();

  Gtk::Button m_button;
  Gtk::Pixmap m_pixmap;
};

Pixmap::Pixmap()
: m_pixmap(xpm_data)
{
  set_border_width(10);

  add(m_button);
  m_button.add(m_pixmap);

  m_button.signal_clicked().connect(slot(*this, &Pixmap::on_button_clicked));
  
  show_all();
}

void on_button_clicked()
{
  std::cout << "button clicked" << endl;
}

int main (int argc, char *argv[])
{
  Gtk::Main myapp(argc, argv);

  Pixmap pixmap;

  Gtk::Main::run(pixmap);
  return 0;
}
