
#include <gtkmm/main.h>
#include <gtkmm/window.h>
#include <gtkmm/drawingarea.h>
#include <gdkmm/colormap.h>
#include <gdkmm/window.h>

class MyArea : public Gtk::DrawingArea
  {
      Glib::RefPtr<Gdk::GC>       gc_;
      Gdk::Color    blue_,red_;
      Glib::RefPtr<Gdk::Window>   window_;
    public:
      MyArea()
        {
          // in the ctor you can only allocate colors, 
          // get_window() returns 0 because we have not be realized
          Glib::RefPtr<Gdk::Colormap> colormap = get_default_colormap ();

          blue_ = Gdk::Color("blue");
          red_ = Gdk::Color("red");

          colormap->alloc_color(blue_);
          colormap->alloc_color(red_);
        }
    protected:

      virtual void on_realize()
        {
          // we need to do the default realize
          Gtk::DrawingArea::on_realize();

          // Now we can allocate any additional resources we need
          window_ = get_window();
          Glib::RefPtr<Gdk::Drawable> drawable = window_; //TODO: I get compiler errors without doing this first..
          gc_ = Gdk::GC::create(drawable);
          window_->set_background(red_);
          window_->clear();
          gc_->set_foreground(blue_);
        }


      virtual bool on_expose_event(GdkEventExpose* e)
        {
          // here is where we draw on the window
          window_->clear();
          window_->draw_line(gc_, 1, 1, 100, 100);
          return true;
        }
  };


int main(int argc, char** argv)
  {
     Gtk::Main kit(argc,argv);
     Gtk::Window win;
     
     MyArea area;
     win.add(area);
     win.show_all();
     
     Gtk::Main::run(win);

     return 0;
  }
