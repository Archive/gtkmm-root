#include <iostream>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/entry.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>

// Gtk-- version of the "entry" example from the gtk+ tutorial

class Entry : public Gtk::Window
{
  Gtk::HBox m_hbox;
  Gtk::VBox m_vbox;
  Gtk::Entry m_entry;
  Gtk::Button m_bClose;
  Gtk::CheckButton m_cbEditable, m_cbVisible;

  void toggleEditable() { m_entry.set_editable(m_cbEditable.get_active()); }
  
  void toggleVisibility() { m_entry.set_visibility(m_cbVisible.get_active()); }
  void enterCallback();
  
public:
  Entry();
};

Entry::Entry() :
  m_hbox(false, 0),
  m_vbox(false, 0),
  m_entry(),
  m_bClose("Close"),
  m_cbEditable("Editable"),
  m_cbVisible("Visible")
{

  set_size_request(200, 100);
  set_title("Gtk-- Entry");
  
  add(m_vbox);
  
  m_entry.signal_activate().connect(slot(*this, &Entry::enterCallback));
  m_entry.set_max_length(50);
  m_entry.set_text("hello");
  m_entry.set_text(m_entry.get_text() + " world");
  m_entry.select_region(0, m_entry.get_text_length());
  m_vbox.pack_start(m_entry);
  
  // Note that add() can also be used instead of pack_xxx()
  m_vbox.add(m_hbox);

  m_hbox.pack_start(m_cbEditable);
  m_cbEditable.signal_toggled().connect(slot(*this, &Entry::toggleEditable));
  m_cbEditable.set_active(true);

  m_hbox.pack_start(m_cbVisible);
  m_cbVisible.signal_toggled().connect(slot(*this,&Entry::toggleVisibility));
  m_cbVisible.set_active(true);

  m_bClose.signal_clicked().connect(slot(*this, &Gtk::Widget::hide));
  m_vbox.pack_start(m_bClose);
  m_bClose.set_flags(Gtk::CAN_DEFAULT);
  m_bClose.grab_default();
  
  show_all();
}

void
Entry::enterCallback()
{
  std::cout << "Entry contents : " << m_entry.get_text() << std::endl;
}

int main (int argc, char *argv[])
{
  Gtk::Main myapp(&argc, &argv);

  Entry entry;

  Gtk::Main::run(entry);
  return 0;
}

