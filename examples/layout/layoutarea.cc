/*
  GTK-- Canvas Demo 

  Copyright 1998-1999 Kasper Peeters.
  Copyright 1999 Karl Nelson

  This program and its components are free software; you can
  redistribute it and/or modify it under the terms of the GNU General
  Public License as published by the Free Software Foundation; either
  version 2, or (at your option) any later version.
 
  This program and its components are distributed in the hope that it
  will be useful, but WITHOUT ANY WARRANTY; without even the implied
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
  the GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License in
  the file COPYING accompanying this program; if not, write to the
  Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

// This program demonstrates how to render on the background of
// a layout widget to use it as a drawing area.  
// 
// This provides for rapid smooth scrolling like you would expect
// from netscape.  It reduces flashing by matching the pixmap and
// background.  To use LayoutArea, derive it and replace the paint
// method with rendering on the background.

#include <iostream.h>
#include <gtkmm/main.h>
#include <gtkmm/button.h>
#include <gtkmm/layout.h>
#include <gtkmm/window.h>
#include <gtkmm/table.h>
#include <gtkmm/scrollbar.h>
#include <gdkmm/color.h>
#include <gdkmm/pixmap.h>

using SigC::slot;

/***********************************************************
***** Layout with double buffered drawing area
***********************************************************/
class LayoutArea : public Gtk::Layout
{
public:
  LayoutArea(Gtk::Adjustment &hadj,Gtk::Adjustment &vadj)
  :Gtk::Layout(hadj,vadj)
  {
   // set event mask so we are sure to get events
   set_events(GDK_EXPOSURE_MASK);
   has_background_ = 0;
  }

  // this is what is filled out by derived classes
  virtual void paint() = 0;

  // flush part of the pixmap to the background
  void flush(int x, int y, int w, int h);

  // reallocate the background map
  void back_alloc();

  void set_background(const Gdk::Color& bg)
  {
    has_background_ = true;
    background_color_ = bg;
  }

  virtual bool on_expose_event(GdkEventExpose* e);
  virtual void on_realize();

protected:
  Glib::RefPtr<Gdk::Pixmap>   pixmap_;
  Glib::RefPtr<Gdk::Pixmap>   bin_pixmap_;
  Glib::RefPtr<Gdk::Pixmap>   null_pixmap_;
  Glib::RefPtr<Gdk::GC>       gc_;
  Gdk::Color background_color_;

  gint bin_width,bin_height;
  bool has_background_;
};

void LayoutArea::flush(int x,int y,int w,int h)
{
  Glib::RefPtr<Gdk::Window> window = get_bin_window();
  Glib::RefPtr<Gdk::Drawable> drawable = pixmap_;
  bin_pixmap_->draw_drawable(gc_, drawable,
                          x, y,
                          x, y, w, h);

  window->set_back_pixmap(bin_pixmap_,false);
  window->clear_area(x,y,w,h);
  window->set_back_pixmap(null_pixmap_,false);
}

void LayoutArea::back_alloc()
{
  Glib::RefPtr<Gdk::Window> window = get_window();
  bin_width = width();
  bin_height = height();
  bin_pixmap_ = Gdk::Pixmap::create(window, width()+20, height()+20);
  if (has_background_)
    window->set_background(background_color_);
  flush(0, 0, width(), height());
}


bool LayoutArea::on_expose_event(GdkEventExpose* e)
{
  if (width() > bin_width || height() > bin_height)
    {back_alloc();}
  else
    {flush(e->area.x, e->area.y,e->area.width, e->area.height);}

  return Gtk::Layout::on_expose_event(e);
}

void LayoutArea::on_realize()
{
  // we need to do the default realize
  Gtk::Layout::on_realize();

  // allocate our resources
  guint width = 0;
  guint height = 0;
  get_size(width, height);
  Glib::RefPtr<Gdk::Window> window = get_window();
  pixmap_ = Gdk::Pixmap::create(window, width, height);
  if (!gc_)
  {
    Glib::RefPtr<Gdk::Drawable> drawable = pixmap_;
    gc_ = Gdk::GC::create(drawable);
  }

  paint();
  back_alloc();
}

/***********************************************************
***** In action
***********************************************************/
class MyLayout : public LayoutArea
{
public:
  MyLayout(Gtk::Adjustment &hadj, Gtk::Adjustment &vadj)
  :LayoutArea(hadj, vadj)
  {
    // must use gtkmm colormap
    Glib::RefPtr<Gdk::Colormap> colormap_ = get_default_colormap ();

    white_ = Gdk::Color("white");
    black_ = Gdk::Color("black");
    red_ = Gdk::Color("red");

    colormap_->alloc_color(white_);
    colormap_->alloc_color(black_);
    colormap_->alloc_color(red_);

    font_ = Pango::FontDescription("fixed");

    // to reduce flashing we will match the background color
    // to that which we will most be using.
    set_background(black_);
  }

  void paint()
  {
    // always remember to clear the pixmap first!
    gc_->set_foreground(red_);
    pixmap_->draw_rectangle(gc_,true,0,0,width(),height());

    // put something on the pixmap
    gc_->set_foreground(white_);
    gc_->set_background(black_);
    pixmap_->draw_line(gc_,0,0,100,100);
    for(unsigned i = 1; i < 20; ++i)
    {      ]
      //TODO: draw_string is deprecated.
      pixmap_->draw_string(font_, gc_, 10, 10 * i, "some text to show how it works!");
    }
  }

protected:
  Gdk::Color white_, black_, red_;
  Pango::FontDescription font_;
};


/***************************************************************
***** Test setup
***************************************************************/

void print_hello()
{
  cout << "hello"<<endl;
}

void print_there()
{
  cout << "there"<<endl;
}

class MyWindow : public Gtk::Window
{
public:
  MyWindow()
  : Gtk::Window()
  {
    Gtk::Table *table_;
    Gtk::Adjustment *hadj_,*vadj_;
    Gtk::Scrollbar *vscroll_,*hscroll_;
    Gtk::Button *b,*c;
    LayoutArea *layout_;

    table_=manage(new Gtk::Table(2,2));

    hadj_ = manage(new Gtk::Adjustment(0,0,20,8,8,5));
    vadj_ = manage(new Gtk::Adjustment(0,0,20,8,8,5));
    layout_ = manage(new MyLayout(*hadj_,*vadj_));

    vscroll_ = manage(new Gtk::VScrollbar(*(layout_->get_vadjustment())));
    hscroll_ = manage(new Gtk::HScrollbar(*(layout_->get_hadjustment())));
    b=manage(new Gtk::Button("hello"));
    c=manage(new Gtk::Button("there"));
    b->clicked().connect(slot(&print_hello));
    c->clicked().connect(slot(&print_there));

    set_title("Layout");
    layout_->put(*b,500,500);
    layout_->put(*c,100,100);
    layout_->set_size(600,600);

    table_->attach(*layout_,0,1,0,1,
      GTK_FILL|GTK_EXPAND,GTK_FILL|GTK_EXPAND);
    table_->attach(*hscroll_,0,1,1,2,GTK_FILL,GTK_FILL);
    table_->attach(*vscroll_,1,2,0,1,GTK_FILL,GTK_FILL);

    add(*table_);
    set_size_request(100,100);
    show_all();
  }

};

int main(int argc, char **argv)
{
  Gtk::Main mymain(argc, argv);

  MyWindow mywindow;
  Gtk::Main::run(mywindow);
}
