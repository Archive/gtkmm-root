//$Id$ -*- c++ -*-

/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "examplewindow.h"

ExampleWindow::ExampleWindow()
: m_Button_Quit(Gtk::Stock::QUIT),
  m_Button_Buffer1("Use buffer 1"),
  m_Button_Buffer2("Use buffer 2")
{
  set_title("Gtk::TextView example");
  set_border_width(5);
  set_default_size(400, 200);


  add(m_VBox);

  //Add the TreeView, inside a ScrolledWindow, with the button underneath:
  m_ScrolledWindow.add(m_TextView);

  //Only show the scrollbars when they are necessary:
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  m_VBox.pack_start(m_ScrolledWindow);

  //Add buttons: 
  m_VBox.pack_start(m_ButtonBox, Gtk::PACK_SHRINK);

  m_ButtonBox.pack_start(m_Button_Buffer1, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Buffer2, Gtk::PACK_SHRINK);
  m_ButtonBox.pack_start(m_Button_Quit, Gtk::PACK_SHRINK);
  m_ButtonBox.set_border_width(5);
  m_ButtonBox.set_spacing(5);
  m_ButtonBox.set_layout(Gtk::BUTTONBOX_END);

  
  //Connect signals:
  m_Button_Quit.signal_clicked().connect( SigC::slot(*this, &ExampleWindow::on_button_quit) );
  m_Button_Buffer1.signal_clicked().connect( SigC::slot(*this, &ExampleWindow::on_button_buffer1) );
  m_Button_Buffer2.signal_clicked().connect( SigC::slot(*this, &ExampleWindow::on_button_buffer2) );
  
  fill_buffers();
  on_button_buffer1();

  show_all_children();
}

void ExampleWindow::fill_buffers()
{
  //Create the first TextBuffer:
  m_refTextBuffer1 = Gtk::TextBuffer::create();
  m_refTextBuffer1->set_text("This is the text from TextBuffer #1.");

  //Create a TagTable:
  Glib::RefPtr<Gtk::TextBuffer::TagTable> refTagTable = m_refTextBuffer1->get_tag_table();
  
  //Apply color to some text.

  //Create the tags:
  Glib::RefPtr<Gtk::TextBuffer::Tag> refTagOrange = Gtk::TextBuffer::Tag::create("example-orange"); //We give it an arbitrary name.
  refTagOrange->property_background() = "orange";
  refTagTable->add(refTagOrange);

  Glib::RefPtr<Gtk::TextBuffer::Tag> refTagBold = Gtk::TextBuffer::Tag::create("example-bold"); //We give it an arbitrary name.
  refTagBold->property_weight() = Pango::WEIGHT_BOLD;
  refTagTable->add(refTagBold);

  //Use a tag:
  m_refTextBuffer1->apply_tag(refTagOrange, m_refTextBuffer1->begin(), m_refTextBuffer1->get_iter_at_offset(4));

  //Fill the second buffer, using the tags as we add the text:
  m_refTextBuffer2 = Gtk::TextBuffer::create(refTagTable); //share the TagTable. A future gtkmm will have a set_tag_table() method.
  m_refTextBuffer2->set_text("This is some alternative text, from TextBuffer #2.");
  m_refTextBuffer2->insert_with_tag(m_refTextBuffer2->end(), "\nAnd here is some more", refTagOrange);
  m_refTextBuffer2->insert_with_tag(m_refTextBuffer2->end(), "\nAnd some more.", refTagBold);
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_button_quit()
{
  hide();
}

void ExampleWindow::on_button_buffer1()
{
  m_TextView.set_buffer(m_refTextBuffer1);
}

void ExampleWindow::on_button_buffer2()
{
  m_TextView.set_buffer(m_refTextBuffer2);
}



