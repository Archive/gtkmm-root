#include "testwindow.h"

int main(int argc, char *argv[])
{
  Gtk::Main main_runner(argc, argv);
  TestWindow foo;
  main_runner.run(foo);
  return 0;
}

