#ifndef GTKMM_EXAMPLE_DRAWINGAREATEXT_H
#define GTKMM_EXAMPLE_DRAWINGAREATEXT_H

#include <gtkmm.h>

class CustomDrawingArea : public Gtk::DrawingArea
{
public:
  CustomDrawingArea(int x_size = 0, int y_size = 0);
  bool on_expose_event(GdkEventExpose* event);
  
protected:
  int m_width, m_height;
};

#endif //GTKMM_EXAMPLE_DRAWINGAREATEXT_H

