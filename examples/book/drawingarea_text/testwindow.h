#ifndef GTKMM_EXAMPLE_DATTESTWIN_H
#define GTKMM_EXAMPLE_DATTESTWIN_H

#include "customdrawingarea.h"

class TestWindow : public Gtk::Window
{
public:
  TestWindow();
private:
  CustomDrawingArea m_drawing_area;
};

#endif

