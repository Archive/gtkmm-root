#include "customdrawingarea.h"
#include <pangomm/layout.h>

CustomDrawingArea::CustomDrawingArea(int x_size, int y_size)
  : DrawingArea(), m_width(x_size), m_height(y_size)
{
  set_size_request(m_width, m_height);

   //TODO: Why do we store m_width and m_height? murrayc
}

//Expose_event method.
bool CustomDrawingArea::on_expose_event(GdkEventExpose*)
{
  Glib::RefPtr<Gdk::Window> win = get_window();
  Glib::RefPtr<Gdk::GC> some_gc = Gdk::GC::create(win);
  Glib::RefPtr<Gdk::GC> blackgc = get_style()->get_black_gc();
  Glib::RefPtr<Gdk::GC> whitegc = get_style()->get_white_gc();
  
  Gdk::Color some_color;
  Glib::RefPtr<Gdk::Colormap> some_colormap = get_default_colormap();
  some_color.set_red(65535);
  some_color.set_green(65535);
  some_color.set_blue(0);
  some_colormap->alloc_color(some_color);
  some_gc->set_foreground(some_color);

  //Draw pac-man.
  win->draw_arc(some_gc, true, 30, 100, 50, 50, 2880, 17280); //2880==45*64, 17280==270*64

  //Draw pellets.
  win->draw_rectangle(whitegc, true, 80, 120, 15, 10);
  win->draw_rectangle(whitegc, true, 110, 120, 15, 10);
  win->draw_rectangle(whitegc, true, 140, 120, 15, 10);

  //Draw some lines.
  win->draw_line(blackgc, 5, 2, 5, 20);
  win->draw_line(blackgc, 5, 11, 10, 11);
  win->draw_line(blackgc, 10, 2, 10, 20);
  win->draw_line(blackgc, 15, 2, 21, 2);
  win->draw_line(blackgc, 18, 2, 18, 20);
  win->draw_line(blackgc, 15, 20, 21, 20);

  //Draw a diamond.
  std::vector<Gdk::Point> some_points;
  some_points.push_back(Gdk::Point(100, 10));
  some_points.push_back(Gdk::Point(50, 60));
  some_points.push_back(Gdk::Point(100, 110));
  some_points.push_back(Gdk::Point(150, 60));
  win->draw_polygon(blackgc, true, some_points);

  //Draw some text.
  Glib::RefPtr<Pango::Layout> pangolayout = create_pango_layout("Hello, World!");
  win->draw_layout(blackgc, 30, 170, pangolayout);

  // return true to stop any other event handlers being called to
  // draw this area
  return true;
}

