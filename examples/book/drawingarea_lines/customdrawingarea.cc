#include "customdrawingarea.h"

CustomDrawingArea::CustomDrawingArea(int x_size, int y_size)
  : DrawingArea()
{
  set_size_request(x_size, y_size);

  //TODO: Why do we store m_width and m_height? murrayc
}

//Expose_event method.
bool CustomDrawingArea::on_expose_event(GdkEventExpose*)
{
  Glib::RefPtr<Gdk::Window> win = get_window();
  Glib::RefPtr<Gdk::GC> gc = get_style()->get_black_gc();
  win->draw_line(gc, 5, 2, 5, 20);
  win->draw_line(gc, 5, 11, 10, 11);
  win->draw_line(gc, 10, 2, 10, 20);
  win->draw_line(gc, 15, 2, 21, 2);
  win->draw_line(gc, 18, 2, 18, 20);
  win->draw_line(gc, 15, 20, 21, 20);

  // return true to stop any further event handlers being called
  // to draw this area
  return true;
}

