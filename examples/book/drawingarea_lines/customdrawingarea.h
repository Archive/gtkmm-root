#ifndef GTKMM_EXAMPLE_DRAWINGAREALINES_H
#define GTKMM_EXAMPLE_DRAWINGAREALINES_H

#include <gtkmm.h>

//Custom drawing area with modified expose_event.
class CustomDrawingArea : public Gtk::DrawingArea
{
public:
  CustomDrawingArea(int x_size = 0, int y_size = 0);
  
  bool on_expose_event(GdkEventExpose* event);
};

#endif //GTKMM_EXAMPLE_DRAWINGAREALINES_H


