#ifndef GTKMM_EXAMPLE_DALTESTWIN_H
#define GTKMM_EXAMPLE_DALTESTWIN_H

#include "customdrawingarea.h"

class TestWindow : public Gtk::Window
{
public:
  TestWindow();
  
protected:
  CustomDrawingArea m_drawing_area;
};

#endif

