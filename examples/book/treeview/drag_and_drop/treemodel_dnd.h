//$Id$ -*- c++ -*-

/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef GTKMM_EXAMPLE_TREEMODEL_DND_H
#define GTKMM_EXAMPLE_TREEMODEL_DND_H

#include <gtkmm.h>

enum typeEnum {
TEST_1,           // Just to test
TEST_2,           // on-the-fly gtypes
TEST_3
};



class TreeModel_Dnd : public Gtk::TreeStore
{
protected:
  explicit TreeModel_Dnd(const Gtk::TreeModelColumnRecord& columns);

public:
  class ModelColumns : public Gtk::TreeModel::ColumnRecord
  {
  public:

    ModelColumns()
    { add(m_col_id); add(m_col_name); add(m_col_draggable); add(m_col_receivesdrags); add(m_col_test); }

    Gtk::TreeModelColumn<int> m_col_id;
    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    Gtk::TreeModelColumn<bool> m_col_draggable;
    Gtk::TreeModelColumn<bool> m_col_receivesdrags;
    Gtk::TreeModelColumn<typeEnum> m_col_test;
  }; 
  
  ModelColumns m_Columns;

  static Glib::RefPtr<TreeModel_Dnd> create(const Gtk::TreeModelColumnRecord& columns);

protected:
  //Overridden virtual functions:
  virtual bool row_draggable_vfunc(const Gtk::TreeModel::Path& path);
  virtual bool row_drop_possible_vfunc(const Gtk::TreeModel::Path& dest, GtkSelectionData* selection_data);
  
};

#endif //GTKMM_EXAMPLE_TREEMODEL_DND_H
