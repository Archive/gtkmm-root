#include <gtkmm/main.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include <iostream>
#include <memory>

//TODO: gcc 2.96RH doesn't have auto_ptr<>:
std::auto_ptr<std::istream> input;


// this will be our callback for read operations
// there is not much to say. just read a string,
// print it and quit the application if the string was quit
void MyCallback(int, Gdk::InputCondition) {
  std::string dummy;
  do {
    (*input) >> dummy;
    std::cout << dummy << std::endl;
    if(dummy == "quit") Gtk::Main::quit();
  } while(input->fail());
}


int main (int argc, char *argv[])
{
  // the usual Gtk::Main object
  Gtk::Main app(argc, argv);

  // create a fifo for testing purposes
  if (mkfifo("testfifo",0666) != 0) { 
    std::cerr << "error creating fifo" << std::endl;
    return -1;
  }
  
  // open the fifo
  input = new std::ifstream("testfifo");

  int fd = open("testfifo", 0);
  if (fd == -1) {
    std::cerr << "error opening fifo" << std::endl;
    return -1;
  }

  // assign the fifo's filedescriptor to our ifstream object
  //This sucks; it will only ever work with libstdc++-v3, as
  //  both istream::__filebuf_type and the basic_filebuf contructor
  //  that takes an fd are libstdc++-v3 specific.
  //input=new istream(new ifstream::__filebuf_type(fd,"testfifo"));
  
  // connect the callback function
  app.signal_input().connect(SigC::slot(MyCallback), fd, GDK_INPUT_READ);

  // and last but not least - run the application main loop
  Gtk::Main::run();

  // now remove the temporary fifo
  if(unlink("testfifo")) 
    std::cerr << "error removing fifo" << std::endl;

  return 0;
}

