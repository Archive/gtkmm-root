/* example-start fixed fixed.c */

#include <gtkmm/button.h>
#include <gtkmm/fixed.h>
#include <gtkmm/window.h>
#include <gtkmm/main.h>

class AppWindow : public Gtk::Window
{
  gint x;
  gint y;

public:
  AppWindow ();
  ~AppWindow ();

  void move_button(Gtk::Fixed*,Gtk::Button*);

};

AppWindow::AppWindow()
:  x(50), y(50)
{
  Gtk::Fixed *fixed;
  Gtk::Button *button;
  gint i;

  /* Create a new window */
  set_title("Fixed Container");

  /* Sets the border width of the window. */
  set_border_width (10);

  /* Create a Fixed Container */
  fixed = manage( new Gtk::Fixed() );
  add(*fixed);
  
  for (i = 1 ; i <= 3 ; i++) {
    /* Creates a new button with the label "Press me" */
    button = manage( new Gtk::Button("Press me") );
  
    /* When the button receives the "clicked" signal, it will call the
     * function move_button() passing it the Fixed Containter as its
     * argument. */
    button->signal_clicked().connect(SigC::bind(SigC::slot(*this,&AppWindow::move_button),fixed,button));
  
    /* This packs the button into the fixed containers window. */
    fixed->put (*button, i*50, i*50);
  }

  show_all ();
}

AppWindow::~AppWindow() {}



/* This callback function moves the button to a new position
 * in the Fixed container. */
void AppWindow::move_button( Gtk::Fixed *fixed,
                  Gtk::Button *button )
{
  x = (x+30)%300;
  y = (y+50)%300;
  fixed->move(*button, x, y); 
}

int main( int   argc,
          char *argv[] )
{
  /* Initialise GTK */
  Gtk::Main app(&argc, &argv);
  AppWindow window;
    
  /* Enter the event loop */
  Gtk::Main::run (window);
    
  return(0);
}
/* example-end */
