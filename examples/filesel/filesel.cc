/* example-start filesel filesel.cc */
#include <iostream>
#include <gtkmm/fileselection.h>
#include <gtkmm/main.h>

//From <iostream> 
using std::cout;
using std::endl;

using SigC::slot;

class filesel: public Gtk::FileSelection
{
public:
    filesel();
private:
    /* Get the selected filename and print it to the console */
    void file_ok_sel() {
	std::cout << "file_ok_sel: " << get_filename() << std::endl;
    }

};

filesel::filesel():
    Gtk::FileSelection("File selection")
{
    /* Connect the ok_button_ to file_ok_sel function */
    get_ok_button()->signal_clicked().connect(slot(*this, &filesel::file_ok_sel));
    /* Connect the cancel_button_ to hiding the window */
    get_cancel_button()->signal_clicked().connect(SigC::slot(*this, &Gtk::Widget::hide));
}


int main (int argc, char *argv[])
{
    /* Initialize GTK-- */
    Gtk::Main m(&argc, &argv);
    
    /* Create a new file selection widget */
    filesel filew;
    
    /* Lets set the filename, as if this were a save dialog, and we are giving
       a default filename */
    filew.set_filename("penguin.png");

    Gtk::Main::run(filew);

    return 0;
}
/* example-end */
