dnl $Id$


define(`_CLASS_INTERFACE',`dnl
_PUSH()
dnl
dnl  Define the args for later macros
define(`__CPPNAME__',`$1')
define(`__CNAME__',`$2')
define(`__CCAST__',`$3')
define(`__CCLASS__',`$4') dnl SomethingIface or SomethingClass, both suffixes are used.
define(`__BASE__',_LOWER(__CPPNAME__))
define(`__CPPPARENT__',`Glib::Interface')
dnl define(`__CPARENT__',`GObject')
define(`__PCAST__',`(GObject*)')
define(`__BOOL_IS_INTERFACE__',`$1')

dnl
dnl Declares and implements the destructor
dnl
define(`_DTOR',`dnl
_PUSH(SECTION_CC)
__CPPNAME__::~__CPPNAME__`'()
{
}
_POP()')


_POP()
_SECTION(SECTION_CLASS2)
') dnl end of _CLASS_INTERFACE


dnl Some of the Gdk types are actually direct typedefs of their base type.
dnl This means that 2 wrap functions would have the same argument.
define(`_NO_WRAP_FUNCTION',`dnl
_PUSH()
dnl Define this macro to be tested for later.
define(`__BOOL_NO_WRAP_FUNCTION__',`$1')
_POP()
')

dnl GdkVisual's GdkVisualClass struct defintion is hidden,
dnl and different for X11, linux-fb, etc,
dnl so we can't derived from it.
dnl This means that we can't wrap signals or virtual funcs,
dnl But there aren't any, so that's OK.
define(`_NO_DERIVED_CLASS',`dnl
_PUSH()
dnl Define this macro to be tested for later.
define(`__BOOL_NO_DERIVED_CLASS__',`$1')
_POP()
')


dnl
dnl
dnl
define(`_PH_CLASS_DECLARATION_INTERFACE',`dnl
class __CPPNAME__`'_Class : public Glib::Class
{
public:

  typedef __CPPNAME__           CppObjectType;
  typedef __CNAME__           BaseObjectType;
ifdef(`__BOOL_NO_DERIVED_CLASS__',`dnl
',`dnl
  typedef __CCLASS__      BaseClassType;
  typedef __CPPPARENT__`'_Class     CppClassParent;
  //typedef __CPARENT__     BaseClassParent;
')dnl

  friend class CppObjectType;

  GType get_type();

ifdef(`__BOOL_NO_DERIVED_CLASS__',`dnl
',`dnl
  static void iface_init_function(BaseClassType* iface);
  static void class_init_function(BaseClassType* klass);
  static void object_init_function(BaseObjectType* object);
')dnl

protected:

  static Glib::Instance_Base* wrap_new(GObject*);

  //Callbacks (default signal handlers):
  //These will call the *_impl member methods, which will then call the existing default signal callbacks, if any.
  //You could prevent the original default signal handlers being called by overriding the *_impl method.
_IMPORT(SECTION_PH_DEFAULT_SIGNAL_HANDLERS)

  //Callbacks (virtual functions):
_IMPORT(SECTION_PH_VFUNCS)
};
')


dnl
dnl
dnl
define(`_PCC_CLASS_IMPLEMENTATION_INTERFACE',`dnl
GType __CPPNAME__`'_Class::get_type()
{
ifdef(`__BOOL_NO_DERIVED_CLASS__',`dnl
  gtype_ = _LOWER(__CCAST__)_get_type(); //The actual struct definition is hidden, so we can not derive one.
',`dnl
  if (!gtype_) //Create the GType if necessary.
    {
      //Make sure that the parent type has been created:
      CppClassParent::CppObjectType::get_type();

      //Get the size of the base C class:
      GType gtype_base = _LOWER(__CCAST__)_get_type();
      gtype_ = gtype_base; //We can not derive from another interface.
/*
      GTypeQuery typeQueryBase = {0 ,};
      g_type_query(gtype_base, &typeQueryBase);

      //Create a derived C type.
      //These init funcs will, in turn, call the init functions in the base C type.
      GTypeInfo info =
        {
          typeQueryBase.class_size, // class_size
          (GBaseInitFunc) 0, // base_init *
          (GBaseFinalizeFunc) 0, // base_finalize
          (GClassInitFunc) class_init_function, // class_init
          0, // class_finalize
          0, // class_data
          typeQueryBase.instance_size, // instance_size
          0, // n_preallocs
          (GInstanceInitFunc) object_init_function // instance_init
        };

      gtype_ = g_type_register_static(gtype_base, "__MODULE__`'__`'__CPPNAME__", &info, GTypeFlags(0));
*/
    }
')dnl
  return gtype_;
}

ifdef(`__BOOL_NO_DERIVED_CLASS__',`dnl
',`dnl
void __CPPNAME__`'_Class::iface_init_function(BaseClassType* iface)
{
  CppClassParent::iface_init_function((GTypeInterface*)iface);
  BaseClassType* klass = iface; //Allows us to reuse generated code from GObject wrappers.
 _IMPORT(SECTION_PCC_CLASS_INIT_VFUNCS)
 _IMPORT(SECTION_PCC_CLASS_INIT_DEFAULT_SIGNAL_HANDLERS)
}

void __CPPNAME__`'_Class::class_init_function(BaseClassType* klass)
{
  CppClassParent::class_init_function((GTypeInterface*)klass);
dnl   _IMPORT(SECTION_PCC_CLASS_INIT_VFUNCS)
dnl   _IMPORT(SECTION_PCC_CLASS_INIT_DEFAULT_SIGNAL_HANDLERS)
}

void __CPPNAME__`'_Class::object_init_function(BaseObjectType* object)
{
dnl _IMPORT(SECTION_PCC_OBJECT_INIT)
}
')dnl

_IMPORT(SECTION_PCC_VFUNCS)

_IMPORT(SECTION_PCC_DEFAULT_SIGNAL_HANDLERS)
')


dnl
dnl _END_CLASS_INTERFACE()
dnl   denotes the end of a class
dnl
define(`_END_CLASS_INTERFACE',`
_SECTION(SECTION_HEADER1)
//typedef struct _`'__CNAME__ __CNAME__;
//typedef struct _`'__CNAME__`'Class __CNAME__`'Class;
_SECTION(SECTION_HEADER3)

ifdef(`__BOOL_NO_WRAP_FUNCTION__',`dnl
',`dnl
namespace Glib { Glib::RefPtr<__NAMESPACE__::__CPPNAME__> wrap(__CNAME__`'* object, bool take_copy = false); }
')dnl

dnl
dnl
_SECTION(SECTION_PHEADER)

#include <glibmm/class.h>

__NAMESPACE_BEGIN__

_PH_CLASS_DECLARATION_INTERFACE()

__NAMESPACE_END__

_SECTION(SECTION_SRC_GENERATED)

ifdef(`__BOOL_NO_WRAP_FUNCTION__',`dnl
',`dnl else
namespace Glib
{

Glib::RefPtr<__NAMESPACE__::__CPPNAME__> wrap(__CNAME__`'* object, bool take_copy /* = false */)
{
  return Glib::RefPtr<__NAMESPACE__::__CPPNAME__>( dynamic_cast<__NAMESPACE__::__CPPNAME__*> (Glib::wrap_auto ((GObject*)(object), take_copy)) );
  //We use dynamic_cast<> in case of multiple inheritance.
}

} /* namespace Glib */
')dnl endif


__NAMESPACE_BEGIN__


/* The *_Class implementation: */

_PCC_CLASS_IMPLEMENTATION_INTERFACE()

Glib::Instance_Base* __CPPNAME__`'_Class::wrap_new(GObject* o)
{
  return new __CPPNAME__`'((__CNAME__*)`'(o));
}


/* The implementation: */

/*
__CNAME__* __CPPNAME__::gobj_copy()
{
  reference();
  return gobj();
}
*/

Glib::RefPtr<__CPPNAME__> __CPPNAME__::wrap_specific_type(__CNAME__* gobject, bool take_copy /* = false */) //static
{
  Glib::RefPtr<__CPPNAME__> refPtr;
  //Check for an existing wrapper:
  __CPPNAME__* pCppObject = dynamic_cast<__CPPNAME__*>(Glib::Instance_Base::_get_current_wrapper(G_OBJECT(gobject)));
  if(pCppObject)
  {
    //Return the existing wrapper:
    refPtr = Glib::RefPtr<__CPPNAME__>(pCppObject);
  }
  else
  {
    //Create a new wrapper:
    refPtr = Glib::RefPtr<__CPPNAME__>( new __CPPNAME__`'(gobject) );
  }

  if(take_copy)
    refPtr->reference();

  return refPtr;
}

__CPPNAME__::__CPPNAME__`'()
{
}


void __CPPNAME__`'::add_interface(GType gtype_implementer) //static
{
  static const GInterfaceInfo iface_info =
  {
	  (GInterfaceInitFunc)CppClassType::iface_init_function,
	  NULL,
	  NULL
  };

  g_type_add_interface_static(
    gtype_implementer,
    get_type(), //interface_type: whose direct parent type must be G_TYPE_INTERFACE
    &iface_info
  );
}

_CC_CLASS_IMPLEMENTATION()

__NAMESPACE_END__

dnl
dnl
dnl
dnl
_POP()

dnl The actual class, e.g. Gtk::Widget, declaration:
class __CPPNAME__`'_Class;
_IMPORT(SECTION_CLASS1)
public:
  typedef __CPPNAME__          CppObjectType;
  typedef __CPPNAME__`'_Class    CppClassType;
  typedef __CNAME__           BaseObjectType;
ifdef(`__BOOL_NO_DERIVED_CLASS__',`dnl
',`dnl
  typedef __CCLASS__      BaseClassType;
')dnl

private:
  friend class CppClassType;
  static CppClassType `'__BASE__`'_class_;

  __CPPNAME__`'(const __CPPNAME__&);
  __CPPNAME__& operator=(const __CPPNAME__&); // not implemented

protected:
  __CPPNAME__`'(); //You must derive from this class.
  explicit __CPPNAME__`'(__CNAME__ *castitem);

public:
  virtual ~__CPPNAME__`'();

  static void add_interface(GType gtype_implementer);

  static GType get_type();
  __CNAME__* gobj()             { return (__CNAME__*)`'(gobject_); }
  const __CNAME__* gobj() const { return (__CNAME__*)`'(gobject_); }

  //__CNAME__* gobj_copy();

  static Glib::RefPtr<__CPPNAME__> wrap_specific_type(__CNAME__* gobject, bool take_copy = false); //Re-uses the existing wrapper, if it exists.

_H_SIGNAL_PROXIES()

_H_VFUNCS_AND_SIGNALS()

private:
_IMPORT(SECTION_CLASS2)

')

