/* $Id$ */

/* texttag.h
 * 
 * Copyright (C) 1998-2002 The gtkmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <gtkmm/object.h>
#include <gtkmm/enums.h>
#include <gdkmm/bitmap.h>
#include <gdkmm/color.h>
#include <pangomm/fontdescription.h>
#include <pangomm/tabarray.h>
#include <gtk/gtktexttag.h>

_DEFS(gtkmm,gtk)
_PINCLUDE(glibmm/private/object_p.h)

namespace Gtk
{

class TextIter;

/**
 * @ingroup TextView
 */
class TextTag : public Glib::Object
{
   _CLASS_GOBJECT(TextTag, GtkTextTag, GTK_TEXT_TAG, Glib::Object, GObject)
protected:
  _CTOR_DEFAULT()
  _WRAP_CTOR(TextTag(const Glib::ustring& name), gtk_text_tag_new)

public:
  _WRAP_CREATE()
  _WRAP_CREATE(const Glib::ustring& name)

  _WRAP_METHOD(int get_priority() const, gtk_text_tag_get_priority)
  _WRAP_METHOD(void set_priority(int priority), gtk_text_tag_set_priority)
  _WRAP_METHOD(bool event(const Glib::RefPtr<Glib::Object>& event_object, GdkEvent* event, const TextIter& iter),
               gtk_text_tag_event)

  _WRAP_SIGNAL(bool event(const Glib::RefPtr<Glib::Object>& event_object, GdkEvent* event, const TextIter& iter), "event")

  //_WRAP_PROPERTY("name", Glib::ustring) //construct-only.
  _WRAP_PROPERTY("background", Glib::ustring)
  _WRAP_PROPERTY("foreground", Glib::ustring)
  _WRAP_PROPERTY("background-gdk", Gdk::Color)
  _WRAP_PROPERTY("foreground-gdk", Gdk::Color)
  _WRAP_PROPERTY("background-stipple", Glib::RefPtr<Gdk::Bitmap>)
  _WRAP_PROPERTY("foreground-stipple", Glib::RefPtr<Gdk::Bitmap>)
  _WRAP_PROPERTY("font", Glib::ustring)
  _WRAP_PROPERTY("font-desc", Pango::FontDescription)
  _WRAP_PROPERTY("family", Glib::ustring)
  _WRAP_PROPERTY("style", Pango::Style)
  _WRAP_PROPERTY("variant", Pango::Variant)
  _WRAP_PROPERTY("weight", int)
  _WRAP_PROPERTY("stretch", Pango::Stretch)
  _WRAP_PROPERTY("size", int)
  _WRAP_PROPERTY("size-points", double)
  _WRAP_PROPERTY("scale", double)
  _WRAP_PROPERTY("pixels-above-lines", int)
  _WRAP_PROPERTY("pixels-below-lines", int)
  _WRAP_PROPERTY("pixels-inside-wrap", int)
  _WRAP_PROPERTY("editable", bool)
  _WRAP_PROPERTY("wrap-mode", WrapMode)
  _WRAP_PROPERTY("justification", Justification)
  _WRAP_PROPERTY("direction", TextDirection)
  _WRAP_PROPERTY("left-margin", int)
  _WRAP_PROPERTY("indent", int)
  _WRAP_PROPERTY("strikethrough", bool)
  _WRAP_PROPERTY("right-margin", int)
  _WRAP_PROPERTY("underline", Pango::Underline)
  _WRAP_PROPERTY("rise", int)
  _WRAP_PROPERTY("background-full-height", bool)
  _WRAP_PROPERTY("language", Glib::ustring)
  _WRAP_PROPERTY("tabs",  Pango::TabArray)
  _WRAP_PROPERTY("invisible", bool)
  _WRAP_PROPERTY("background-set", bool)
  _WRAP_PROPERTY("foreground-set", bool)
  _WRAP_PROPERTY("background-stipple-set", bool)
  _WRAP_PROPERTY("foreground-stipple-set", bool)
  _WRAP_PROPERTY("family-set", bool)
  _WRAP_PROPERTY("style-set", bool)
  _WRAP_PROPERTY("variant-set", bool)
  _WRAP_PROPERTY("weight-set", bool)
  _WRAP_PROPERTY("stretch-set", bool)
  _WRAP_PROPERTY("size-set", bool)
  _WRAP_PROPERTY("scale-set", bool)
  _WRAP_PROPERTY("pixels-above-lines-set", bool)
  _WRAP_PROPERTY("pixels-below-lines-set", bool)
  _WRAP_PROPERTY("pixels-inside-wrap-set", bool)
  _WRAP_PROPERTY("editable-set", bool)
  _WRAP_PROPERTY("wrap-mode-set", bool)
  _WRAP_PROPERTY("justification-set", bool)
  _WRAP_PROPERTY("left-margin-set", bool)
  _WRAP_PROPERTY("indent-set", bool)
  _WRAP_PROPERTY("strikethrough-set", bool)
  _WRAP_PROPERTY("right-margin-set", bool)
  _WRAP_PROPERTY("underline-set", bool)
  _WRAP_PROPERTY("rise-set", bool)
  _WRAP_PROPERTY("background-full-height-set", bool)
  _WRAP_PROPERTY("language-set", bool)
  _WRAP_PROPERTY("tabs-set", bool)
  _WRAP_PROPERTY("invisible-set", bool)
};

} /* namespace Gtk */

