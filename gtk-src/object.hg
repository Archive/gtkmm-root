/* $Id$ */

/* Copyright (C) 1998-2002 The gtkmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm/object.h>
#include <gtkmm/base.h>
#include <gtkmmconfig.h>
_DEFS(gtkmm,gtk)
_PINCLUDE(glibmm/private/object_p.h)

namespace Gtk
{

using SigC::manage; //Makes manage available as Gtk::manage().

class Object : public Glib::Object
{
  _CLASS_GTKOBJECT(Object,GtkObject,GTK_OBJECT,Glib::Object,GObject)
  _IGNORE(gtk_object_ref, gtk_object_unref, gtk_object_weakref, gtk_object_weakunref, gtk_object_set_data,
          gtk_object_set_data_full, gtk_object_remove_data, gtk_object_get_data, gtk_object_remove_no_notify,
          gtk_object_set_user_data, gtk_object_get_user_data, gtk_object_set_data_by_id, gtk_object_set_data_by_id_full,
          gtk_object_get_data_by_id, gtk_object_remove_data_by_id, gtk_object_remove_no_notify_by_id, gtk_object_get,
          gtk_object_set, gtk_object_add_arg_type, gtk_object_destroy, gtk_object_sink)
  _CUSTOM_DTOR
  _CUSTOM_CTOR_CAST

public:
  //void shutdown(); //We probably don't need this.
  //void finalize(); //We probably don't need this.

  //void set_user_data(gpointer data);
  //gpointer get_user_data();

  virtual void set_manage();

  _WRAP_PROPERTY("user-data", void*)

  bool is_managed_() const;

protected:

  void destroy_();

  // If you need it, give me an example. murrayc. -- Me too. daniel.
  //_WRAP_SIGNAL(void destroy(), "destroy")
  _IGNORE_SIGNAL(destroy)

  void _init_unmanage(bool is_toplevel = false);
  virtual void destroy_notify_(); //override.
  void disconnect_cpp_wrapper();
  void _destroy_c_instance();
  static void callback_destroy_(GObject* gobject, void* data); //only connected for a short time.

  // set if flags used by derived classes.
  bool referenced_; // = not managed.
  bool gobject_disposed_;
};

} // namespace Gtk

