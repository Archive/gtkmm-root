/* $Id$ */

/* Copyright(C) 1998-2002 The gtkmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or(at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

_DEFS(gtkmm,gtk)
_PINCLUDE(glibmm/private/object_p.h)

#include <gtkmm/treeiter.h>
#include <gtkmm/treemodel.h>
#include <gtkmm/treesortable.h>
// We couldn't include it in treemodel.h, but doing it here makes it easier for people.
#include <gtkmm/treepath.h>


namespace Gtk
{

/**
 * @ingroup TreeView
 */
class TreeModelSort : public Glib::Object, public TreeModel, public TreeSortable
{
  _CLASS_GOBJECT(TreeModelSort, GtkTreeModelSort, GTK_TREE_MODEL_SORT, Glib::Object, GObject)
  _IMPLEMENTS_INTERFACE(TreeModel)
  _IMPLEMENTS_INTERFACE(TreeSortable)

protected:
  _WRAP_CTOR(TreeModelSort(const Glib::RefPtr<TreeModel>& model), gtk_tree_model_sort_new_with_model)

public:
  _WRAP_CREATE(const Glib::RefPtr<TreeModel>& model)

  _WRAP_METHOD(Glib::RefPtr<TreeModel> get_model(), gtk_tree_model_sort_get_model, refreturn)
  _WRAP_METHOD(Glib::RefPtr<const TreeModel> get_model() const, gtk_tree_model_sort_get_model, refreturn)

  _WRAP_METHOD(TreeModel::Path convert_child_path_to_path(const Path& child_path) const,
               gtk_tree_model_sort_convert_child_path_to_path)

  iterator convert_child_iter_to_iter(const iterator& child_iter) const;

  _WRAP_METHOD(TreeModel::Path convert_path_to_child_path(const Path& sorted_path) const,
               gtk_tree_model_sort_convert_path_to_child_path)

  iterator convert_iter_to_child_iter(const iterator& sorted_iter) const;

  _WRAP_METHOD(void reset_default_sort_func(), gtk_tree_model_sort_reset_default_sort_func)
  _WRAP_METHOD(void clear_cache(), gtk_tree_model_sort_clear_cache)

  _WRAP_METHOD(bool iter_is_valid(const iterator& iter) const, gtk_tree_model_sort_iter_is_valid)

protected:
  virtual void set_value_impl(const iterator& row, int column, const Glib::ValueBase& value);
};

} // namespace Gtk

